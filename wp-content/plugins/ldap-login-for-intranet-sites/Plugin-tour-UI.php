<?php
function plugin_tour_ui() {
?>
    <div id="mo_ldap_settings" >
        <form name="f" method="post" id="show_ldap_pointers">
            <input type="hidden" name="option" value="clear_ldap_pointers"/>
            <input type="hidden" name="restart_tour" id="restart_tour"/>
            <input type="hidden" name="restart_plugin_tour" id="restart_plugin_tour"/>
        </form></div>

        <div class="mo_ldap_local_main_head" style="margin-top: 3rem;">
			<div class="mo_ldap_title_container">
                <div>
                    <img src="<?php echo esc_url(plugin_dir_url( __FILE__ ) . 'includes/images/logo.png'); ?>"  width="50" height="50">
                </div>
                <div class="mo_ldap_local_title">
                    miniOrange LDAP/Active Directory Login for Intranet Sites
                </div>
            </div>
            <div style="display:flex; justify-content: flex-end;">
                <a id="ldap_trial_for_premium_plugin" class="button button-large button-request-trial" href="<?php echo esc_url(add_query_arg( array('tab' => 'trial_request'), $_SERVER['REQUEST_URI'] )); ?>">Full-Featured Trial</a>
                <a id="license_upgrade" class="button button-primary button-large button-licensing-plans" href="<?php echo esc_url(add_query_arg( array( 'tab' => 'pricing' ), htmlentities( $_SERVER['REQUEST_URI'] ) )); ?>">Licensing Plans</a>
                <a style="" id="configure-restart-plugin-tour" type="button" value="restart_plugin_tour" class="button button-restart-full-tour button-large"  onclick="jQuery('#restart_plugin_tour').val('true');jQuery('#show_ldap_pointers').submit();"><em class="fas fa-sync"></em> Take Plugin Tour</a>
            </div>  
		</div>
    <?php

    if(!MoLdapLocalUtil::is_extension_installed('ldap')) {
        ?>
        <div class="notice notice-error is-dismissible">
            <p style="color:#FF0000">Warning: PHP LDAP extension is not installed or disabled.</p>
            <div id="help_ldap_warning_title" class="mo_ldap_title_panel">
                <p><a target="_blank" style="cursor: pointer;">Click here for instructions to enable it.</a></p>
            </div>
            <div hidden="" style="padding: 2px 2px 2px 12px" id="help_ldap_warning_desc" class="mo_ldap_help_desc">
                <ul>
                    <li style="font-size: large; font-weight: bold">Step 1 </li>
                    <li style="font-size: medium; font-weight: bold">Loaded configuration file : <?php echo esc_attr(php_ini_loaded_file()) ?></li>
                    <li style="list-style-type:square;margin-left:20px">Open php.ini file from above file path</li><br/>
                    <li style="font-size: large; font-weight: bold">Step 2</li>
                    <li style="font-weight: bold;color: #C31111">For Windows users using Apache Server</li>
                    <li style="list-style-type:square;margin-left:20px">Search for <strong>"extension=php_ldap.dll"</strong> in php.ini file. Uncomment this line, if not present then add this line in the file and save the file.</li>
                    <li style="font-weight: bold;color: #C31111">For Windows users using IIS server</li>
                    <li style="list-style-type:square;margin-left:20px">Search for <strong>"ExtensionList"</strong> in the php.ini file. Uncomment the <strong>"extension=php_ldap.dll"</strong> line, if not present then add this line in the file and save the file.</li>
                    <li style="font-weight: bold;color: #C31111">For Linux users</li>
                        <ul style="list-style-type:square;margin-left: 20px">
                            <li style="margin-top: 5px">Install php ldap extension (If not installed yet)
                                <ul style="list-style-type:disc;margin-left: 15px;margin-top: 5px">
                                    <li>For Ubuntu/Debian, the installation command would be <strong>sudo apt-get -y install php-ldap</strong></li>
                                    <li>For RHEL based systems, the command would be <strong>yum install php-ldap</strong></li></ul></li></li>
                            <li>Search for <strong>"extension=php_ldap.so"</strong> in php.ini file. Uncomment this line, if not present then add this line in the file and save the file.</li></ul><br/>
                    <li style="margin-top: 5px;font-size: large; font-weight: bold">Step 3</li>
                    <li style="list-style-type:square;margin-left:20px">Restart your server. After that refresh the "LDAP/AD" plugin configuration page.</li>
                </ul>
                <strong>For any further queries, please contact us.</strong>
            </div>
            <p style="color:black">If your site is hosted on <strong>Shared Hosting</strong> and it is impossible you to enable the extension then you can use our <strong><a href="https://wordpress.org/plugins/miniorange-wp-ldap-login/" rel="noopener" target="_blank" style="cursor: pointer;">Active Directory/LDAP Integration for Cloud & Shared Hosting Platforms</a></strong>.</p>
        </div>
        <?php
    }
    if(!MoLdapLocalUtil::is_extension_installed('openssl')) {
        ?>
        <div class="notice notice-error is-dismissible">
        <p style="color:#FF0000">(Warning: <a target="_blank" rel="noopener" href="http://php.net/manual/en/openssl.installation.php">PHP OpenSSL extension</a> is not installed or disabled)</p>
        </div>
        <?php
    }
?>
    <div class="new-div">
        <div class="nav-new">
            <table class="mo_ldap_local_nav_table">
                <td id="ldap_default_tab_pointer" class="mo_ldap_local_active_tab" > <a href="<?php echo esc_url(add_query_arg( array('tab' => 'default'), $_SERVER['REQUEST_URI'] )); ?>"><div style="padding: 6px 0;"">LDAP<br>Configuration</div></a> </td>
                <td id="ldap_role_mapping_tab_pointer" class="" > <a href="<?php echo esc_url(add_query_arg( array('tab' => 'rolemapping'), $_SERVER['REQUEST_URI'] )); ?>"><div style="padding: 6px 0;"">Role<br>Mapping</div></a> </td>
                <td id="ldap_attribute_mapping_tab_pointer" class="" > <a href="<?php echo esc_url(add_query_arg( array('tab' => 'attributemapping'), $_SERVER['REQUEST_URI'] )); ?>"><div style="padding: 6px 0;"">Attribute<br>Mapping</div></a> </td>
                <td id="ldap_signin_settings_tab_pointer" class="" > <a href="<?php echo esc_url(add_query_arg( array('tab' => 'signin_settings'), $_SERVER['REQUEST_URI'] )); ?>"><div style="padding: 6px 0;"">Sign-In<br>Settings</div></a> </td>
                <td id="ldap_multiple_directories_tab_pointer" class="" > <a href="<?php echo esc_url(add_query_arg( array('tab' => 'multiconfig'), $_SERVER['REQUEST_URI'] )); ?>"><div style="padding: 6px 0;"">Multiple<br>Directories</div></a> </td>
                <td id="ldap_config_settings_tab_pointer" class="" > <a href="<?php echo esc_url(add_query_arg( array('tab' => 'config_settings'), $_SERVER['REQUEST_URI'] )); ?>"><div style="padding: 6px 0;"">Configuration<br>Settings</div></a> </td>
                <td id="ldap_User_Report_tab_pointer" style="position: relative;" class="" > <a href="<?php echo esc_url(add_query_arg( array('tab' => 'Users_Report'), $_SERVER['REQUEST_URI'] )); ?>"><div style="padding: 6px 0;"">Authentication<br>Report</div></a> </td>
                <td id="ldap_add_on_tab_pointer" class="" > <a href="<?php echo esc_url(add_query_arg( array('tab' => 'addons'), $_SERVER['REQUEST_URI'] )); ?>"><div style="padding: 16px 0;"">Add-Ons</div></a> </td>
            </table>
            </div>
            <table class="mo_ldap_local_table" aria-hidden="true">
                <tr>
                <td style="width:74%;vertical-align:top; padding-top: 54px; border-spacing:0;" id="configurationForm">
                    <div style="border-top: 4px solid #ff7776; border-radius: 5px;">
                        <div id="ldap_configuration_tab" style="display: block">
                            <?php mo_ldap_local_configuration_page(); ?>
                        </div>

                        <div id="troubleshooting_tab" style="display: none">
                            <?php mo_ldap_local_troubleshooting(); ?>
                        </div>

                        <div id="signin_settings_tab" style="display: none;">
                            <?php mo_ldap_local_signin_settings(); ?>
                        </div>

                        <div id="ldap_multiple_directories_tab" style="display: none;">
                            <?php mo_ldap_local_multiple_ldap(); ?>
                        </div>

                        <div id="role_mapping_tab" style="display: none;">
                            <?php mo_ldap_local_rolemapping(); ?>
                        </div>

                        <div id="registration_tab" style="display: none">
                            <?php if (get_option ( 'mo_ldap_local_verify_customer' ) == 'true') {
                                mo_ldap_show_verify_password_page_ldap();
                            } elseif (! MoLdapLocalUtil::is_customer_registered()) {
                                mo_ldap_show_new_registration_page_ldap();
                            } else{
                                mo_ldap_show_customer_details();
                            }?>
                        </div>

                        <div id="attribute_mapping_tab" style="display: none">
                            <?php mo_ldap_show_attribute_mapping_page(); ?>
                        </div>

                        <div id="export_tab" style="display: none">
                            <?php mo_show_export_page(); ?>
                        </div>
						
						 <div id="Users_Report" style="display: none">
                            <?php mo_user_report_page(); ?>
                        </div>

                        <div id="add_ons" style="display: none">
                            <?php mo_ldap_local_add_on_page(); ?>
                        </div>


                        <div id="feature_request_tab" style="display:none;">
                            <?php mo_ldap_local_support(); ?>
                        </div>
                    </div>
                    </td>
                    
                    <td id='support_block' style="vertical-align:top;padding-left:1%; border-spacing:0;width: 26%;">
                        <div class="mo_ldap_quick_links_container mo_ldap_support_layout1">
                            <div class="mo_ldap_local_support_header">
                                <img src="<?php echo esc_url(plugin_dir_url( __FILE__ ) . 'includes/images/addon-images/quicklink.png'); ?>" alt="">
                                <h3 class="quick-links-text" style="font-size: 25px; margin-top: 5px;">Quick Links</h3>
                                
                            </div>
                            <div class="mo_ldap_local_title_btns_container">
                                <div class="row">
                                    <div class="col span-1-of-2">
                                        <a id="ldap_trial_for_premium_plugin" class="button button-primary-ldap button-large button-quick-links" href="<?php echo esc_url(add_query_arg( array('tab' => 'trial_request'), $_SERVER['REQUEST_URI'] )); ?>">Request for Trial</a>
                                    </div>
                                    <div class="col span-1-of-2">
                                        <a id="ldap_feature_request_tab" class="button button-primary-ldap button-large button-quick-links" href="<?php echo esc_url(add_query_arg( array('tab' => 'ldap_feature_request'), $_SERVER['REQUEST_URI'] )); ?>">Feature Request</a>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col span-1-of-2">
                                        <a id="ldap_troubleshooting_tab_pointer" class="button button-primary-ldap button-large button-quick-links" href="<?php echo esc_url(add_query_arg( array('tab' => 'troubleshooting'), $_SERVER['REQUEST_URI'] )); ?>">FAQ's</a>
                                    </div>
                                    <div class="col span-1-of-2">
                                        <a id="ldap_account_setup_tab_pointer" class="button button-primary-ldap button-large button-quick-links" href="<?php echo esc_url(add_query_arg( array('tab' => 'account'), $_SERVER['REQUEST_URI'] )); ?>">My Account</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php 
                        mo_ldap_local_support();
                        ?>
                    </td>
                </tr>
            </table>
    </div>
    <div class='overlay_back' id="overlay" hidden></div>
<?php } ?>