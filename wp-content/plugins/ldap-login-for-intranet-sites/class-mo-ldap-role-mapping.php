<?php

class MoLdapLocalRoleMapping{

	function mo_ldap_local_update_role_mapping($user_id,$new_registered_user) {
		if($user_id==1) {
			return;
		}

		$roles = 0;
        $wpuser = new WP_User($user_id);
        $default_role = !empty(get_option('mo_ldap_local_mapping_value_default')) ? get_option('mo_ldap_local_mapping_value_default'):'subscriber';
        $keep_existing_roles = get_option('mo_ldap_local_keep_existing_user_roles');

        if ($roles == 0) {
            if (isset($default_role)) {
                if ($keep_existing_roles == 1 && !$new_registered_user) {
                    $wpuser->add_role($default_role);
                } else {
                    $wpuser->set_role($default_role);
                }
            }
        }
	}
}
?>