<?php


namespace rnpdfimporter\ajax;


use rnpdfimporter\pr\Utilities\Activator;

class Settings extends AjaxBase
{

    function GetDefaultNonce()
    {
        return 'settings';
    }

    protected function RegisterHooks()
    {
        $this->RegisterPrivate('activate_license','ActivateLicense');
        $this->RegisterPrivate('deactivate_license','DeactivateLicense');

    }


    public function ActivateLicense(){

        $licenseKey=$this->GetRequired('LicenseKey');
        $expirationDate=$this->GetRequired('ExpirationDate');
        $url=$this->GetRequired('URL');
        (new Activator())->SaveLicense($this->Loader,$licenseKey,$expirationDate,$url);
        $this->SendSuccessMessage('');
    }

    public function DeactivateLicense(){
        Activator::DeleteLicense($this->Loader);

        $this->SendSuccessMessage('');
    }
}