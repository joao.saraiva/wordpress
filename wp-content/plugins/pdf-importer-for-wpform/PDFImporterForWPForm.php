<?php

/**
 * Plugin Name: PDF Importer for WPForm
 * Plugin URI: http://smartforms.rednao.com/getit
 * Description: Import a pdf and fill with with your form.
 * Author: RedNao
 * Author URI: http://rednao.com
 * Version: 1.3.19
 * Text Domain: PDF Importer WPForm
 * Domain Path: /languages/
 * License: GPLv3
 * License URI: http://www.gnu.org/licenses/gpl-3.0
 * Slug: pdf-importer-for-wpform
 */

use rnpdfimporter\core\Integration\Adapters\WPForm\Loader\WPFormSubLoader;
use rnpdfimporter\core\Loader;
require_once dirname(__FILE__).'/AutoLoad.php';

new WPFormSubLoader('rnpdfimporter','rednaopdfimpwpform',24,9,basename(__FILE__),array(
    'ItemId'=>16,
    'Author'=>'Edgar Rojas',
    'UpdateURL'=>'https://pdfimporter.rednao.com',
    'FileGroup'=>'PDFImporterForWPForm'
));