<?php return array(
    'root' => array(
        'name' => 'wpfluent/wpfluent',
        'pretty_version' => 'dev-master',
        'version' => 'dev-master',
        'reference' => '970e6ab4d6db211a48be4b827e71566267dd009f',
        'type' => 'project',
        'install_path' => __DIR__ . '/../../',
        'aliases' => array(
            0 => '1.0.x-dev',
        ),
        'dev' => true,
    ),
    'versions' => array(
        'wpfluent/framework' => array(
            'pretty_version' => '1.2.1',
            'version' => '1.2.1.0',
            'reference' => '7359c1eba0b440896cef1aa2d6ce89767c5cd96f',
            'type' => 'library',
            'install_path' => __DIR__ . '/../wpfluent/framework',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'wpfluent/wpfluent' => array(
            'pretty_version' => 'dev-master',
            'version' => 'dev-master',
            'reference' => '970e6ab4d6db211a48be4b827e71566267dd009f',
            'type' => 'project',
            'install_path' => __DIR__ . '/../../',
            'aliases' => array(
                0 => '1.0.x-dev',
            ),
            'dev_requirement' => false,
        ),
    ),
);
