=== Ninja Charts- WordPress Charts and Graphs Plugin ===


Contributors: wpmanageninja, adreastrian, csesumonpro, hasanuzzamanshamim
Tags: charts, chart, graphs, graph, table, Data Tables, WP Charts, WordPress Chart Plugin, Grid, csv data, Tablular Data, spreadsheet data
Requires at least: 5.2
Requires PHP: 5.6
Tested up to: 6.1
Stable tag: 3.2.0
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html


== Description ==


You are at the right place if you are here searching for a chart creator plugin to create responsive, customizable, beautiful charts for your WordPress website.


We introduce Ninja Charts for you as a data visualizer solution for your websites. It has amazing features that will help you create unlimited online charts & graphs on your web pages.


Ninja Charts is a powerful and lightweight chart generator that allows you to create and use numerous types of charts in WordPress. Its JavaScript-based chart rendering engine makes the chart creation process easier than ever.


### Ninja Charts Features


<ul><li>Deep integration with [Ninja Tables](https://wordpress.org/plugins/ninja-tables/)</li>
<li>Easy-to-use interface</li>
<li>Design and Layout customization</li>
<li>Responsive, mobile-friendly WordPress chart plugin</li>
<li>Simple and handy steps to get started</li>
<li>SEO-friendly charts</li>
<li>Shortcodes to embed</li>
<li>Live preview on admin</li>
<li>Multiple chart types available</li>
<li>Multiple data sources</li>
<li>Versatile rendering engines</li>
<li>Special formatting and preview option</li>
<li>Amazing, dedicated support facility</li></ul>


### How Does It Work?


It’s the easiest chart-maker solution for your websites. You just need to specify the titles and the chart type, then you will find the option to put a value manually, or you can retrieve data from Ninja Tables plugin directly.


After completing the above-mentioned process, the plugin will automatically generate a short code for a single chart, which you can embed on any page or post to show the respective data.


Currently, the plugin offers different styles of charts that you can adjust with your required data in a visual representation.


- Line charts
- Pie charts
- Bar charts
- Bubble charts
- Doughnut charts
- Radar charts
- Polar charts
- Scatter charts
- Area charts
- Combo charts
- Horizontal bar charts


### Ninja Tables Integration With Ninja Charts


One of the most prominent features of [Ninja Charts](https://wordpress.org/plugins/ninja-tables/) is its ability to retrieve data from popular data table builder: [Ninja Tables](https://ninjatables.com) and show them in the form of different charts.


When it is packed with Ninja Tables, it is one of WordPress's most eligible chart-maker plugins.


With this WordPress charts plugin, you can create charts retrieving the data from the Ninja Tables. To create charts from Ninja Tables, take a look at the simple steps as follows-


**Step 1:** After installing the Ninja Charts, click on the Add New button


**Step 2:** Put your chart name in the title ➟ select chart render engine ➟ choose your preferred chart type from the dashboard ➟ and click on the **Next** button


**Step 3:** From the Data Source, select Ninja Tables ➟ Click on the **Next** button [In this step, you will also have an option for manual chart creation.]


**Step 4:** Now, choose the expected table that you want to create a chart. [For manual chart creation, you’ll get an option to define it manually]


**Step 5:** Then, you’ll get the option to drag & drop the desired columns and click on the **Next** button.


**Step 6:** Next, here you will get the prepared chart with customization options in the **Formatting & preview**. Once you’re done with the necessary customization, click on the **Next** button.


**Step 7:** Once you click on the **Next** button, you’re done with the process, and you will see a short-code-based chart has been created.


The steps are easy-going and save you tons of time.


### Unlimited Customization


Ninja Charts allows you to customize your data chart in the best possible way. While creating the chart, in the **Formatting and preview** step, you will get a bunch of advanced options to customize your online chart
. Here, you can customize-
- Chart width
- Responsive chart width
- Chart height
- Background color
- Border width
- Border color
- Border radius
- Font size
- Font style
- Font color
Moreover, you’ll get more options like **AXES**, **TITLE**, **TOOLTIP**, and **LEGEND** to make your online chart more personalized.


### Awesome Support


We care about your needs and would like to serve your purposes in the best possible way. Our dedicated support team and developers are always ready to cooperate with your queries.


If you have any queries, feel free to open a [ticket](https://wpmanageninja.com/support-tickets/). We would love to get back to you shortly.


== Installation ==


This section describes how to install Ninja Charts and get it working.
e.g.
Install using one of these options:
**Method 1:**Install directly from the WordPress Admin panel: go to Plugins -> Add New -> Search for “Ninja Charts,” and click the Install button.
**Method 2:**Download the ZIP manually from WordPress’ plugins repository and upload it through the WordPress Admin panel: go to Plugins -> Add New -> Upload Plugin, browse to the downloaded Zip and upload it.
**Method 3:**Download the ZIP, extract it and manually upload the extracted folder through FTP to the /wp-content/plugins/ directory of your WordPress installation.


== Screenshots ==


1. Fresh Ninja Charts admin panel
2. Chart creation interface
3. Ninja Charts support multiple data sources e.g. Ninja Tables, Fluent Forms
4. Manual data source interface
5. Numerous Chart customization
6. Renderable shortcode
7. Admin dashboard to manage various charts
8. Rendered chart in the frontend


== Changelog ==


= 3.2.0 (Date: Nov 30, 2022) =
* Added frontend preview option
* Added skeleton loader
* Added warning message for an empty column of ninja-tables
* Updated UI design
* Fixes text input duplicate column issue for manual data source
* Fixes data source delete issue for ninja-tables & fluentforms
* Fixes specific table column delete issue


= 3.1.2 (Date: Aug 30, 2022) =
* Added discrete line chart option from empty value
* Fixes data update issue for manual input
* Fixes empty cell values on chart
* Fixes form validation warning
* Fixes pie, polar & donut chart series colour changing issue
* Fixes data sanitization and esc_* functions issue


= 3.1.1 (Date: Dec 21, 2021) =
* Added google pie type charts font size
* Added google charts pie, donut font color option
* Improves ph@8.0 support
* Fixes short-code rendering issue from another plugins
* Fixes google charts title show/hide issue in frontend
* Fixes render chart in oxygen page builder
* Fixes character length issue for large chart name
* Fixes csv import issue column data type not found
* Fixes google charts legend issue in frontend


= 3.1.0 (Date: Jan 27, 2021) =
* Adds google charts
* Adds chart.js new chart type "Combo Chart"
* Adds 10 types of google charts
* Adds data order ascending or descending
* Improved backend code structure
* Optimizes code for faster rendering
* Fixes fluent form range-picker data render issue


= 3.0.0 (Date: Dec 2, 2020) =
* Adds new Fluent Forms fields i.e. Radio, Checkbox,
Dropdown, Select, Multi-Select, Country (Select),
Country(from address), Net Promoter, Ratings,
GDPR checkbox, Range slider, Payment Quantity and Phone
* Adds new Ninja Tables fields Select-field, Remote Google Sheets table
* Adds percentage label for specific charts
* Adds points customization for line/area chart
* Adds line chart thickness customization
* Adds chart render animation
* Improves UI/UX
* Improves backend query
* Optimizes code for faster rendering
* Fixes large scale datasets issue (tested up to 50k)
* Fixes Bubble chart hover issue
* Fixes Chart Series bug
* Fixes render time with loader


= 2.0 (Date: Sep 12, 2020) =
* Adds new data source "Fluent Forms"
* Adds New chart type "Horizontal Bar"
* Adds classic editor support
* Adds Gutenberg editor support
* Adds data range picker (i.e. items, date)
* Adds Rest API
* Adds customization for the Manual Chart
* Adds chart customization with various chart.js features (i.e. shared tooltip, fill area etc.)
* Improved "Ninja Tables" integration
* Optimized code with clean UI
* Fixed various bugs and overall improves performance


= 1.0 (Date: Jul 08, 2020) =
* Init first version
